package com.example.cryclapp.ui.single_movie_details

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.bumptech.glide.Glide
import com.example.cryclapp.Favourite
import com.example.cryclapp.FavouriteDatabase
import com.example.cryclapp.R
import com.example.cryclapp.data.api.POSTER_BASE_URL
import com.example.cryclapp.data.api.TheMovieDBClient
import com.example.cryclapp.data.api.TheMovieDBInterface
import com.example.cryclapp.data.repo.NetworkState
import com.example.cryclapp.data.repo.popinterface
import com.example.cryclapp.data.vo.MovieDetails
import com.example.cryclapp.data.vo.movie_search
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_single_movie.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.NumberFormat
import java.util.*

class SingleMovie : AppCompatActivity() {

    private lateinit var viewModel: SingleMovieViewModel
    private lateinit var movieRepository: MovieDetailsRepository

    val api_key:String="0e03d86efe00ea1a1e1dd7d2a4717ba1"
    var maxLimit : Int =996
    val retrofit= Retrofit.Builder().baseUrl("https://api.themoviedb.org/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    val baseURL = "https://image.tmdb.org/t/p/w780/"
    val service=retrofit.create(popinterface::class.java)
    var movieId: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_movie)

        val apiService : TheMovieDBInterface = TheMovieDBClient.getClient()
        movieRepository = MovieDetailsRepository(apiService)
        movieId= intent.getIntExtra("id",1)

        viewModel = getViewModel(movieId)

        viewModel.movieDetails.observe(this, Observer {
            bindUI(it)
        })

        viewModel.networkState.observe(this, Observer {
            progress_bar.visibility = if (it == NetworkState.LOADING) View.VISIBLE else View.GONE
            txt_error.visibility = if (it == NetworkState.ERROR) View.VISIBLE else View.GONE

        })


        val db: FavouriteDatabase by lazy {
            Room.databaseBuilder(
                this,
                FavouriteDatabase::class.java,
                "Fav.db"
            ).allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
        }

        share.setOnClickListener {
            val intent= Intent()
            intent.action=Intent.ACTION_SEND
            intent.putExtra(Intent.EXTRA_TEXT,"Hey Check out this Great app:")
            intent.type="text/plain"
            startActivity(Intent.createChooser(intent,"Share To:"))
        }


        addtofav.setOnClickListener {

                service.getmovies(movieId,api_key).enqueue(object : Callback<movie_search> {
                    override fun onFailure(call: Call<movie_search>, t: Throwable) {
                        Log.d("MoviesDagger", t.toString())
                    }


                    override fun onResponse(call: Call<movie_search>, response: Response<movie_search>) {

                        val data=response.body()
                        if(response.isSuccessful) {
                            var obj= Favourite(movie_id = movieId.toString(),path = data!!.poster_path.toString())
                            db.FavDao().insertRow(obj)
                            // imgfav.setImageResource(R.drawable.ic_favorite_fill)
                            Toast.makeText(this@SingleMovie, "Added to favourite", Toast.LENGTH_SHORT).show()
                        }


                    }
                })
            }







            service.getmovies(movieId,api_key).enqueue(object : Callback<movie_search> {
                override fun onFailure(call: Call<movie_search>, t: Throwable) {
                    Log.d("MoviesDagger", t.toString())
                }


                override fun onResponse(call: Call<movie_search>, response: Response<movie_search>) {

                    val data=response.body()





                    if (data != null) {
                        Picasso.get().load(baseURL+data.backdrop_path).resize(413,200).into(iv_movie_poster)
                        Picasso.get().load(baseURL+data.poster_path).into(iv_movie_poster)
                    }
                    movie_title.text=data?.original_title
                    movie_release_date.text="Release Date    " +data?.release_date
                    movie_rating.text=data?.vote_average+"/10"
                    movie_overview.text=data?.overview


                }
            })
        }




    fun bindUI( it: MovieDetails) {
        movie_title.text = it.title
        movie_tagline.text = it.tagline
        movie_release_date.text = it.releaseDate
        movie_rating.text = it.rating.toString()
        movie_runtime.text = it.runtime.toString() + " minutes"
        movie_overview.text = it.overview

        val formatCurrency = NumberFormat.getCurrencyInstance(Locale.US)
        movie_budget.text = formatCurrency.format(it.budget)
        movie_revenue.text = formatCurrency.format(it.revenue)

        val moviePosterURL = POSTER_BASE_URL + it.posterPath
        Glide.with(this)
            .load(moviePosterURL)
            .into(iv_movie_poster);

        val id = intent.getStringExtra("id")?.toInt()

        Log.d("id", movieId.toString())


    }


    private fun getViewModel(movieId:Int): SingleMovieViewModel {
        return ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return SingleMovieViewModel(movieRepository,movieId) as T
            }
        })[SingleMovieViewModel::class.java]
    }
}
