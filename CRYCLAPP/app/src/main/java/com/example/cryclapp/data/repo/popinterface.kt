package com.example.cryclapp.data.repo



import com.example.crycl.Model.*
import com.example.cryclapp.Favourite
import com.example.cryclapp.data.vo.MovieResponse
import com.example.cryclapp.data.vo.movie_search
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface popinterface {

    @GET("3/movie/{id}")
    fun getmovies(

        @Path("id") id:Int,
        @Query("api_key") key: String
    ) :Call<movie_search>

    @GET("3/search/person")
    fun getSearchPeople(
        @Query("api_key") key : String,
        @Query("query") query : String
    ) : Call<Searchresonse>

    @GET("3/search/movie")
    fun getSearchMovie(
        @Query("api_key") key : String,
        @Query("query") query : String
    ) : Call<Searchresonse>

    @GET("3/search/tv")
    fun getSearchTv(
        @Query("api_key") key : String,
        @Query("query") query : String
    ) : Call<Searchresonse>


}
